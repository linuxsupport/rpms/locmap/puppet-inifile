Name:           puppet-inifile
Version:        2.1
Release:        1%{?dist}
Summary:        Puppetlabs inifile module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Puppetlabs inifile module, used by eosclient puppet module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/inifile/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/inifile/
touch %{buildroot}/%{_datadir}/puppet/modules/inifile/supporting_module

%files -n puppet-inifile
%{_datadir}/puppet/modules/inifile

%changelog
* Tue Oct 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 2.1-1
- Rebased to #48d41ae4 by locmap-updater

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.0-4
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0-3
- fix requires on puppet-agent

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
- Rebuild for el8

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5 release

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Tue Jul 5 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Initial release
